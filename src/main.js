import Vue from 'vue'
import App from './App.vue'
import Home from '../src/views/Home.vue'
import router from './router'
import './assets/css/materialize.css'
import './assets/js/materialize.js'
// import './assets/js/script.js'

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  router,
  render: function (h) { return h(App) }
}).$mount('#app')
