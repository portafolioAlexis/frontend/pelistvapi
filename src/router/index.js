import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Movie from '../views/Movie.vue'
import DetailsMovie from '../views/DetailsMovie.vue'
import Profile from '../views/Profile.vue'

Vue.use(VueRouter)

  const routes = [
    { path: '/', name: 'home', component: Home },
    { path: '/Movie',name: 'movie',component: Movie },
    {
      path: '/movie/:id/detailsmovie', name: 'movie.details', component: DetailsMovie,
      props: (route) => ({ id: Number(route.params.id) }),
    },
    { path: '/about', name: 'about',component: About },
    { path: '/profile', name: 'Profile',component: Profile },
    { path: "*", component: () => import("../views/NotFound.vue"),} // catch all 404 - define at the very end
  ]

const router = new VueRouter({
  routes,
  base:'/',
  mode:'history'
})

export default router
